package com.school.nav.service.elasticSearch.impl;

import com.school.nav.dao.elasticSearch.SearchResourceRepository;
import com.school.nav.entity.SysResources;
import com.school.nav.service.elasticSearch.SearchResourceService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.SearchHits;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * FileName: SearchResourceServiceImplTest
 * Author: qinshixing
 * Data: 2022/3/5 0005
 * Description:
 */
@SpringBootTest
class SearchResourceServiceImplTest {

    @Autowired
    private SearchResourceService searchResourceService;
    @Autowired
    private SearchResourceRepository searchResourceRepository;
    @Test
    void FindById() {
        SysResources byId = searchResourceService.findById("2775511827944046593");
        //List<SysResources> qsx = searchResourceService.findByUserId("2774567881491349504");
        //System.out.println(qsx.get(0));
        System.out.println(byId);


//        try {
//            searchResourceRepository.deleteById(Long.valueOf("2774543022958837760"));
//            System.out.println("删除成功");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }

    @Test
    void search() {
//        SearchHits<SysResources> search = searchResourceService.search(new String[]{"java"}, false, false, true, 0);
//
//        System.out.println(search);

        List<SysResources> byStatus = searchResourceService.findByStatus(1, 0);
        byStatus.forEach((e) -> System.out.println(e));
    }


}