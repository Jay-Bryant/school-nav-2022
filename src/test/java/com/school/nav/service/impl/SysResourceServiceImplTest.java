package com.school.nav.service.impl;

import com.baomidou.mybatisplus.annotation.TableField;
import com.school.nav.entity.SysResources;
import com.school.nav.service.SysResourceService;
import com.school.nav.utils.SnowflakeUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;


/**
 * FileName: SysResourceServiceImplTest
 * Author: qinshixing
 * Data: 2022/3/5 0005
 * Description:
 */
@SpringBootTest
class SysResourceServiceImplTest {

    @Autowired
    private SysResourceService sysResourceService;

    @Autowired
    private SnowflakeUtil snowflakeUtil;
    @Test
    void insert() {

        String[] tags = new String[]{"通信工程", "计算机", "书籍", "工具", "算法", "java", "前端", "后端", "阿里", "腾讯", "字节跳动", "百度",
                "云计算", "网络", "系统", "Linux"};

        for (int i = 0; i <5 ; i++) {
            SysResources sysResources = new SysResources();
            sysResources.setUserId(snowflakeUtil.nextId());
            System.out.println(sysResources);
            sysResourceService.insert(sysResources);
        }


    }

    @Test
    void ll() throws InterruptedException {
        String[] tags = new String[]{"通信工程", "计算机", "书籍", "工具", "算法", "java", "前端", "后端", "阿里", "腾讯", "字节跳动", "百度",
                "云计算", "网络", "系统", "Linux"};
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            StringBuffer stringBuffer = new StringBuffer();
            SysResources sysResources = new SysResources();
            List<String> tags1 = getTags(tags);
            String[] tag = new String[tags1.size()];
//            tags1.forEach((e)->stringBuffer.append(e+"|"));
            sysResources.setId(snowflakeUtil.nextId());
            sysResources.setCreatDate(new Date());
            sysResources.setCollectNums(random.nextInt(100));

            sysResources.setTag(tags1.toArray(tag));

            int num = i % 2 == 0 ? 0 : 1;
            sysResources.setPerfect(num);
            sysResourceService.insert(sysResources);
            Thread.sleep(1000);
        }

    }

    List<String> getTags(String[] tagsArray) {
        Random random = new Random();
        int anInt = random.nextInt(6)+1;
        List<String > list = new ArrayList();
        for (int i = 0; i < anInt; i++) {
            list.add(tagsArray[random.nextInt(tagsArray.length)]);
        }
        return list;
    }

    @Test
    void update() {
        sysResourceService.updateResourceStatus("2776272307386056704", 3);

    }
}