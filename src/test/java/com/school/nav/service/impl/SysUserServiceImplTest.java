package com.school.nav.service.impl;

import com.school.nav.entity.SysUser;
import com.school.nav.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * FileName: SysUserServiceImplTest
 * Author: qinshixing
 * Data: 2022/3/4 0004
 * Description:
 */
@SpringBootTest
@Slf4j
class SysUserServiceImplTest {

    @Autowired
    private SysUserService sysUserService;
    @Test
    void test() {
        SysUser user = new SysUser();
        user.setId(1499403720281464834l);
        user.setUserRoleId(2);
        System.out.println(user);
        sysUserService.updateByUser(user);
    }

    @Test
    void getById() {
        SysUser user = sysUserService.selectById("1499403720281464834");
        System.out.println(user);
    }

}