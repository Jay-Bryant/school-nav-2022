package com.school.nav.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.nav.entity.IpBlock;
import org.springframework.stereotype.Repository;

/**
 * FileName: IpBlockDao
 * Author: qinshixing
 * Data: 2022/3/9 0009
 * Description: IpBlock持久层
 */
@Repository
public interface IpBlockDao extends BaseMapper<IpBlock> {
}
