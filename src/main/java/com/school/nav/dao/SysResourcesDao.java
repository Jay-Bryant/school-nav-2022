package com.school.nav.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.nav.entity.SysResources;
import org.springframework.stereotype.Repository;

/**
 * 资源表单
(SysResources)表数据库访问层
 *
 * @author makejava
 * @since 2022-03-02 21:01:38
 */
@Repository
public interface SysResourcesDao extends BaseMapper<SysResources> {

}