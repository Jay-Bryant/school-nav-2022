package com.school.nav.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.nav.entity.LikeOfResources;
import org.springframework.stereotype.Repository;

/**
 * FileName: LikeResourcesDao
 * Author: qinshixing
 * Data: 2022/3/3 0003
 * Description: 收藏持久层
 */
@Repository
public interface LikeResourcesDao extends BaseMapper<LikeOfResources> {

}
