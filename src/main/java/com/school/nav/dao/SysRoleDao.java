package com.school.nav.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.nav.entity.SysRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 系统权限表(SysRole)表数据库访问层
 *
 * @author makejava
 * @since 2022-03-01 15:36:12
 */
@Repository
public interface SysRoleDao extends BaseMapper<SysRole> {


}