package com.school.nav.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.nav.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户信息表(SysUser)表数据库访问层
 *
 * @author makejava
 * @since 2022-02-27 19:38:05
 */
@Repository
public interface SysUserDao  extends BaseMapper<SysUser> {
//
//    /**
//     * 通过ID查询单条数据
//     *
//     * @param userId 主键
//     * @return 实例对象
//     */
//    @Results({
//            @Result(property = "userId", column = "user_id"),
//            @Result(property = "deptId", column = "dept_id"),
//            @Result(property = "loginName", column = "login_name"),
//            @Result(property = "userName", column = "user_name"),
//            @Result(property = "userRoleId", column = "user_role_id"),
//            @Result(property = "email", column = "email"),
//            @Result(property = "phoneNumber", column = "phonenumber"),
//            @Result(property = "sex", column = "sex"),
//            @Result(property = "salt", column = "salt"),
//            @Result(property = "status", column = "status"),
//            @Result(property = "loginIp", column = "login_ip"),
//            @Result(property = "loginDate", column = "login_date"),
//            @Result(property = "creatDate", column = "creat_date"),
//            @Result(property = "updateDate", column = "update_date"),
//            @Result(property = "remark", column = "remark"),
//            @Result(property = "avatar", column = "avatar"),
//    })
//    @Select("SELECT * FROM sys_user where")
//    SysUser queryById(long userId);

//    /**
//     * 查询指定行数据
//     *
//     * @param offset 查询起始位置
//     * @param limit 查询条数
//     * @return 对象列表
//     */
//    List<SysUser> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);
//
//
//    /**
//     * 通过实体作为筛选条件查询
//     *
//     * @param sysUser 实例对象
//     * @return 对象列表
//     */
//    List<SysUser> queryAll(SysUser sysUser);
//
//    /**
//     * 新增数据
//     *
//     * @param sysUser 实例对象
//     * @return 影响行数
//     */
//    int insert(SysUser sysUser);
//
//    /**
//     * 修改数据
//     *
//     * @param sysUser 实例对象
//     * @return 影响行数
//     */
//    int update(SysUser sysUser);
//
//    /**
//     * 通过主键删除数据
//     *
//     * @param userId 主键
//     * @return 影响行数
//     */
//    int deleteById(Object userId);

}