package com.school.nav.dao.elasticSearch;

import com.school.nav.entity.SysResources;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * FileName: SearchSheetService
 * Author: qinshixing
 * Data: 2022/3/4 0004
 * Description: 万能资源表单搜索引擎
 */
@Repository
public interface SearchResourceRepository extends ElasticsearchRepository<SysResources, Long> {

    SysResources findById(String id);

    List<SysResources> findByUserId(String userId);

    List<SysResources> findByLink(String link);

    List<SysResources> findByStatus(Integer status, Pageable pageable);





}
