package com.school.nav.dao.elasticSearch;

import com.school.nav.entity.SysUser;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * FileName: SearchUserService
 * Author: qinshixing
 * Data: 2022/3/4 0004
 * Description: 用户搜索引擎
 */
@Repository
public interface SearchUserRepository extends ElasticsearchRepository<SysUser, Long> {

    /**
     * By loginName
     * @param loginName
     * @return
     */
    SysUser findByLoginName(String loginName);
}
