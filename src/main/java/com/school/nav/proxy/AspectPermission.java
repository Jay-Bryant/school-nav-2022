package com.school.nav.proxy;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * FileName: AspectPermission
 * Author: qinshixing
 * Data: 2022/3/23 0023
 * Description: AOP 切入权限代理
 */
@Component
@Aspect
public class AspectPermission {


    @Pointcut("@annotation(com.school.nav.proxy.ApiPermission)")  //通过注解来切入点判断
    public void point() {

    }


    @Before("point()")
    public void before() {
        System.out.println("自定义注解 执行前执行中");
    }
}
