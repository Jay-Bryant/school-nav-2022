package com.school.nav.scheduler;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * FileName: SchedulerTask
 * Author: qinshixing
 * Data: 2022/3/1 0001
 * Description: 定时任务
 */
@Component
public class SchedulerTask {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedDelay = 5000)
    public void process() {
        System.out.println("现在时间：" + dateFormat.format(new Date()));
    }
}
