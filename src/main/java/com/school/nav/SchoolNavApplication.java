package com.school.nav;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import javax.annotation.PostConstruct;

/**
 * FileName: SchoolNavApplication
 * Author: qinshixing
 * Data: 2022/2/1 000114:27
 * Description: 启动类
 */
@MapperScan("com.school.nav.dao")
@SpringBootApplication
@EnableCaching(proxyTargetClass = true)
@EnableElasticsearchRepositories(basePackages = "com.school.nav.dao.elasticSearch")
//@EnableScheduling  // 开启定时任务
public class SchoolNavApplication {

    @PostConstruct
    public void init() {
        // 解决netty启动冲突的问题（主要体现在启动redis和elasticsearch）
        // 可以看Netty4Util.setAvailableProcessors(..)
        System.setProperty("es.set.netty.runtime.available.processors", "false");
    }

    public static void main(String[] args) {
        SpringApplication.run(SchoolNavApplication.class, args);
    }
}
