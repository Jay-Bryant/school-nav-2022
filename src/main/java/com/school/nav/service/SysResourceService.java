package com.school.nav.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.school.nav.entity.SysResources;

/**
 * FileName: SysResourceService
 * Author: qinshixing
 * Data: 2022/3/2 0002
 * Description: 资源表单服务接口 管理员和用户查自己的上传文件接口
 * 访问量较少    数据search 用elasticsearch
 */
public interface SysResourceService {

    /**
     * 通过Id获取资源  加入redis
     * @param resourcesId
     * @return
     */
    SysResources selectById(String  resourcesId);

    /**
     * 通过作者ID查其所有的资源
     * @param userId 作者ID
     * @param status 资源状态 0 拒绝 1审核中 2 已发布
     * @return 默认分页 10
     */
    IPage<SysResources> selectListByAuthorId(String  userId, int status,int current);

    /**
     * 通过标签多参 创建时间排序 收藏数排序
     * @param tagParams 标签多参
     * @param creatTime 创建参数
     * @param collectNums 收藏数
     * @param perfect 精选
     * @return
     */
    IPage<SysResources> searchListByParam(String[] tagParams, Boolean creatTime, Boolean collectNums,Boolean perfect);

    /**
     * 添加资源
     * @return 0 false 1 true
     */
    int insert(SysResources sysResources);

    /**
     * 删除资源
     * @param id
     * @return
     */
    int deleteById(String id);


    /**
     * 管理员设置资源状态
     *
     * @param status
     */
    void updateResourceStatus(String resourceId, int status);

}
