package com.school.nav.service.elasticSearch.impl;

import com.school.nav.dao.elasticSearch.SearchResourceRepository;
import com.school.nav.entity.SysResources;
import com.school.nav.service.elasticSearch.SearchResourceService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * FileName: SearchResourceServiceImpl
 * Author: qinshixing
 * Data: 2022/3/5 0005
 * Description: SearchResourceService实现类
 */
@Service
@Slf4j
public class SearchResourceServiceImpl implements SearchResourceService {

    @Autowired
    private SearchResourceRepository searchResourceRepository;

    @Autowired
    private ElasticsearchRestTemplate restTemplate;

    @Value("${searchPage}")
    private int searchPage;


    @Override
    public SysResources findById(String id) {
        Optional<SysResources> sysResource = null;
        try {
            sysResource = searchResourceRepository.findById(Long.parseLong(id));
            log.info("SearchResourceService 查询 id = {} 成功",id);
        } catch (Exception e) {
            log.info("SearchResourceService 查询 id = {} 失败",id);
            e.printStackTrace();
        }
        return sysResource == null ? null : sysResource.get();
    }

    @Override
    public List<SysResources> findByUserId(String userId) {
        List<SysResources> userResources = searchResourceRepository.findByUserId(userId);
        if (!userResources.isEmpty())
            return userResources;
        return null;
    }

    @Override
    public SearchHits<SysResources> search(String[] tags, Boolean sortByTime, Boolean sortByCollection, Boolean perfect, int currentPage) {

        // 查询条件构建器
        NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder();

        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        for (int i = 0; i < tags.length; i++) {
            boolQuery.must(QueryBuilders.termQuery("tag", tags[i]));
        }
        if (perfect)
            boolQuery.must(QueryBuilders.termQuery("perfect", 1));

        boolQuery.must(QueryBuilders.termQuery("status", 1));  //设置 资源状态为审核成功的
        searchQueryBuilder.withQuery(boolQuery);

        if (sortByTime)
            searchQueryBuilder.withSorts(SortBuilders.fieldSort("creatTime").order(SortOrder.DESC));  //默认设置降序
        if (sortByCollection)
            searchQueryBuilder.withSorts(SortBuilders.fieldSort("collectNums").order(SortOrder.DESC)); //默认设置降序
        //设置分页
        searchQueryBuilder.withPageable(PageRequest.of(currentPage, searchPage));
        SearchHits<SysResources> search = restTemplate.search(searchQueryBuilder.build(), SysResources.class);
        return search;

    }

    /**
     * 线程安全的
     * @param link
     * @return
     */
    @Override
    public synchronized List<SysResources> findByLink(String link) {
        List<SysResources> resources = searchResourceRepository.findByLink(link);
        if (resources == null)
            return null;
        return resources;
    }

    @Override
    public List<SysResources> findByStatus(Integer status,int currentPage) {
        List<SysResources> sysResourcesPage = searchResourceRepository.findByStatus(status, PageRequest.of(currentPage, searchPage));
//        ArrayList<SysResources> list = new ArrayList<>();
//        sysResourcesPage.forEach((e)->{
//            list.add(e);
//        });
        return sysResourcesPage;
    }
}
