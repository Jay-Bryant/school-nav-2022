package com.school.nav.service.elasticSearch;

import com.school.nav.entity.SysResources;
import org.springframework.data.elasticsearch.core.SearchHits;

import java.util.List;

/**
 * FileName: SearchResourceService
 * Author: qinshixing
 * Data: 2022/3/5 0005
 * Description: 资源搜索接口 elasticSearch
 */
public interface SearchResourceService {

    /**
     * ES -> findById
     * @param id
     * @return
     */
    SysResources findById(String id);

    /**
     * ES -> findByUserId
     * @param userId
     * @return
     */
    List<SysResources> findByUserId(String userId);

    /**
     * 资源搜索
     *
     * @param tags             标签
     * @param sortByTime       排序时间
     * @param sortByCollection 排序收藏
     * @param perfect          是否精选
     * @param currentPage      当前页
     * @return
     */
    SearchHits<SysResources> search(String[] tags, Boolean sortByTime, Boolean sortByCollection, Boolean perfect, int currentPage);


    /**
     * 资源链接查
     * 是否为重复资源
     * @param link
     * @return
     */
    List<SysResources> findByLink(String link);


    /**
     * 管理员 审核资源 待审核资源 状态码：0
     */
    List<SysResources> findByStatus(Integer status, int currentPage);

}
