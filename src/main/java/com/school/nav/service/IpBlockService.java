package com.school.nav.service;

import com.school.nav.entity.IpBlock;

import java.util.List;

/**
 * FileName: IpBlockService
 * Author: qinshixing
 * Data: 2022/3/9 0009
 * Description: IpBlock接口服务类
 */
public interface IpBlockService {

    /**
     * 单个插入封杀的Ip
     * @param ip
     */
    void insertIpBlock(String ip);

    /**
     * 链表插入
     *
     * @param blockList
     */
    void insertByIpList(List<String > blockList);

    /**
     * 查询封杀的ip
     * @param ip
     * @return
     */
    Boolean findByIp(String ip);

    /**
     * 取消封杀
     * @param ip
     * @return
     */
    Boolean deleteByIp(String ip);


}
