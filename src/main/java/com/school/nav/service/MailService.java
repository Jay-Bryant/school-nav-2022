package com.school.nav.service;

/**
 * FileName: MailService
 * Author: qinshixing
 * Data: 2022/3/1 0001
 * Description: mail服务
 */
public interface MailService  {

    /**
     * 邮箱发送
     * @param to
     * @param subject
     * @param content
     */
    void sendSimpleMail(String to, String subject, String content);

}
