package com.school.nav.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.school.nav.entity.SysUser;

import java.util.Date;
import java.util.List;

/**
 * FileName: SysUserService
 * Author: qinshixing
 * Data: 2022/2/28 0028
 * Description:
 */
public interface SysUserService {

    /**
     * ID查询用户
     * @param id
     * @return
     */
    SysUser selectById(String id);

    /**
     * 单一参数查询用户
     * @param column
     * @param val
     * @return
     */
    SysUser selectOne(String column,String val);

    /**
     * 查所有的用户 不建议使用
     * @return
     */
    List<SysUser> selectList();

    /**
     * 多参数分页接口
     * 分析：
     *      返回类型： IPage<UserVo> or List<UserVo>  建议IPage  里面整合了其他方法
     *      学习IPage里的方法
     * @param creatDate
     * @param status
     * @param sex
     * @param deptId
     * @return
     */
    IPage<SysUser> selectPage( int current, int size, Date creatDate, Character status, Character sex, Long deptId);


    /**
     * 新增用户
     * @param sysUser
     * @return
     */
    int insert(SysUser sysUser);


    /**
     * 查询用户的数量
     * @return
     */
    int selectUserCount();


    /**
     * 用户更新
     * @param sysUser
     */
    SysUser updateByUser(SysUser sysUser);
}
