package com.school.nav.service;

import com.school.nav.entity.SysRole;
import java.util.List;

/**
 * 系统权限表(SysRole)表服务接口
 *
 * @author makejava
 * @since 2022-03-01 15:36:12
 */
public interface SysRoleService {


    /**
     * 返回权限字符
     * @param roleId
     * @return
     */
    String getRoleByRoleId(int roleId);

    /**
     * 获取权限集合
     * @param roleId
     * @return
     */
    List<String> getRoleList(int roleId);

}