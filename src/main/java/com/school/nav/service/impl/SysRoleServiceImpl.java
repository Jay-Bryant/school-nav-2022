package com.school.nav.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.school.nav.entity.SysRole;
import com.school.nav.dao.SysRoleDao;
import com.school.nav.service.SysRoleService;
import com.school.nav.utils.RoleUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统权限表(SysRole)表服务实现类
 *
 * @author makejava
 * @since 2022-03-01 15:36:12
 */
@Service("sysRoleService")
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private SysRoleDao sysRoleDao;

    @Override
    public String getRoleByRoleId(int roleId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("sys_role_id", roleId);
        SysRole sysRole = sysRoleDao.selectOne(queryWrapper);
        return sysRole.getRoles();
    }
    @Override
    public List<String> getRoleList(int roleId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("sys_role_id", roleId);
        SysRole sysRole = sysRoleDao.selectOne(queryWrapper);
        return RoleUtil.getStringList(sysRole.getRoles());
    }

}