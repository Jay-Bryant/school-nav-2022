package com.school.nav.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.school.nav.dao.SysResourcesDao;
import com.school.nav.dao.elasticSearch.SearchResourceRepository;
import com.school.nav.entity.SysResources;
import com.school.nav.service.SysResourceService;
import com.school.nav.utils.SnowflakeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.document.Document;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.UpdateQuery;
import org.springframework.data.elasticsearch.core.query.UpdateResponse;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Optional;

/**
 * FileName: SysResourceServiceImpl
 * Author: qinshixing
 * Data: 2022/3/3 0003
 * Description:
 */
@Slf4j
@Service("SysResourceService")
public class SysResourceServiceImpl implements SysResourceService {

    @Autowired
    private SysResourcesDao sysResourcesDao;

    @Autowired
    private SearchResourceRepository searchResourceRepository;

    @Autowired
    private ElasticsearchRestTemplate restTemplate;

    @Autowired
    private SnowflakeUtil snowflakeUtil;

    @Value("${defaultPage}")
    public int pageSize;    //分页大小

    @Value("${VERIFICATION_STATUS}")
    private int VERIFICATION_STATUS;

    @Value("${PASS_STATUS}")
    private int PASS_STATUS;

    @Value("${FAIL_STATUS}")
    private int FAIL_STATUS;

    @Override
    public SysResources selectById(String resourcesId) {

        try {
            SysResources resources = sysResourcesDao.selectById(resourcesId);  //数据库
            Optional<SysResources> byId = searchResourceRepository.findById(Long.getLong(resourcesId));
            SysResources sysResources = byId.get();


            if (!ObjectUtils.isEmpty(resources))
                return resources;
        } catch (Exception e) {
            log.info("SysResourceServiceImpl  selectById 错误{}");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public IPage<SysResources> selectListByAuthorId(String userId, int status,int current) {

        QueryWrapper wrapper = new QueryWrapper<>().eq("user_id", userId).eq("status", status);
        Page page = sysResourcesDao.selectPage(new Page<>(current, pageSize), wrapper);
        if (page.getTotal()>0)   //数据总数
            return page;
        return null;
    }

    /**
     * 突然发现 sql不好解决 elasticSearch可以
     * @param tagParams 标签多参
     * @param creatTime 创建参数
     * @param collectNums 收藏数
     * @param perfect 精选
     * @return
     */
    @Override
    public IPage<SysResources> searchListByParam(String[] tagParams, Boolean creatTime, Boolean collectNums, Boolean perfect) {

        QueryWrapper wrapper = new QueryWrapper();

        return null;
    }

    /**
     * 资源插入 并同步到ES中
     * @param sysResources
     * @return
     */
    @Override
    public int insert(SysResources sysResources) {

        long id = snowflakeUtil.nextId();
        sysResources.setId(id);
        sysResources.setStatus(VERIFICATION_STATUS);
        int insert = sysResourcesDao.insert(sysResources);
        if (insert > 0) {
            try {
                searchResourceRepository.save(sysResourcesDao.selectById(id));
                log.info("searchResourceRepository -> save -> #{}",sysResources);
                return 1;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    /**
     * 删除资源数据
     * @param id
     * @return 0 false 1 true
     */
    @Override
    public int deleteById(String id) {
        try {
            sysResourcesDao.deleteById(id);
            log.info("数据库删除资源 id -> #{}",id);
            searchResourceRepository.deleteById(Long.valueOf(id));
            log.info("ES删除资源id -> #{}", id);
            return 1;
        } catch (Exception e) {
            log.info("数据资源 SysResources 删除失败");
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void updateResourceStatus(String resourceId,int status) {
        SysResources sysResources = new SysResources();
        sysResources.setId(Long.parseLong(resourceId));
        sysResources.setStatus(status);

        Document document = Document.create();
        document.putIfAbsent("status", status); //更新后的内容
        UpdateQuery updateQuery = UpdateQuery.builder(resourceId)  //ID
                .withDocument(document)
                .withRetryOnConflict(5)
                .withDocAsUpsert(true)
                .build();
        UpdateResponse response = null;
        try {
            response = restTemplate.update(updateQuery, IndexCoordinates.of("resources_data"));

            try {
                sysResourcesDao.updateById(sysResources);
                log.info("数据库 updateResourceStatus 更新成功");
            } catch (Exception e) {
                log.info("数据库 updateResourceStatus 更新失败");
                e.printStackTrace();
            }
            log.info("ES updateResourceStatus {}",response.getResult().toString()); //UPDATED 表示更新成功
        } catch (Exception e) {
            log.info("ES updateResourceStatus 更新失败");
            e.printStackTrace();
        }
    }

}
