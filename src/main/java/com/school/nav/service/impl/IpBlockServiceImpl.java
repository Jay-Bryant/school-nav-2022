package com.school.nav.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.school.nav.dao.IpBlockDao;
import com.school.nav.entity.IpBlock;
import com.school.nav.entity.SysUser;
import com.school.nav.service.IpBlockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * FileName: IpBlockServiceImpl
 * Author: qinshixing
 * Data: 2022/3/9 0009
 * Description: IpBlockService接口实现类
 */
@Slf4j
@Service
public class IpBlockServiceImpl implements IpBlockService {

    @Autowired
    private IpBlockDao ipBlockDao;


    @Override
    public void insertIpBlock(String ip) {
        IpBlock ipBlock = new IpBlock();
        ipBlock.setIp(ip);
        ipBlockDao.insert(ipBlock);
        log.info("添加封杀的IP: {}",ip);
    }

    @Override
    public void insertByIpList(List<String> blockList) {
        IpBlock ipBlock = new IpBlock();
        blockList.forEach((e) -> {
            ipBlock.setIp(e);
            ipBlockDao.insert(ipBlock);
            log.info("添加封杀的IP: {}",e);
        });
    }

    @Override
    public Boolean findByIp(String ip) {
        IpBlock ipBlock = ipBlockDao.selectOne(new QueryWrapper<IpBlock>().lambda().eq(IpBlock::getIp, ip));
        if (ipBlock != null)
            return true;
        return false;
    }

    @Override
    public Boolean deleteByIp(String ip) {
            ipBlockDao.delete(new QueryWrapper<IpBlock>().lambda().eq(IpBlock::getIp, ip));
            return true;
    }
}
