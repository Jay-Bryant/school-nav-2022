package com.school.nav.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.school.nav.dao.SysUserDao;
import com.school.nav.entity.SysUser;
import com.school.nav.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

/**
 * FileName: SysUserServiceImpl
 * Author: qinshixing
 * Data: 2022/2/28 0028
 * Description:
 */
@Slf4j
@Service("sysUserService")
@CacheConfig(cacheNames = "SysUser_All")
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserDao sysUserDao;

    @Cacheable(value = "SysUserById",key = "'user_'+#id",condition = "#id != null")
    @Override
    public SysUser selectById(String  id) {
        log.info("SysUserService -> selectById -> :{}",id);
        SysUser sysUser = sysUserDao.selectById(id);
        if(sysUser != null)
            return sysUser;
        return null;
    }

    @Cacheable(value = "SysUserList",key = "userist")
    @Override
    public List<SysUser> selectList() {
        log.info("SysUserService -> selectList");
        return sysUserDao.selectList(null);
    }

    @Override
    public SysUser selectOne(String column,String val) {
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper();
        queryWrapper.eq(column, val);
        try {
            SysUser sysUser = sysUserDao.selectOne(queryWrapper);
            if (sysUser != null)
                return sysUser;
        } catch (Exception e) {
            log.info(this.getClass()+"selectOne"+"数据获取错误");
            return new SysUser();
        }
        return null;
    }

    @Override
    public IPage<SysUser> selectPage(int current, int size, Date creatDate, Character status, Character sex, Long deptId) {

        QueryWrapper<SysUser> queryWrapper = new QueryWrapper();
        if (creatDate != null)
            queryWrapper.lambda().ge(SysUser::getCreatDate, creatDate);
        if (status != null)
            queryWrapper.lambda().eq(SysUser::getStatus, status);
        if (sex != null)
            queryWrapper.lambda().eq(SysUser::getSex, sex);
        if (deptId != null)
            queryWrapper.lambda().eq(SysUser::getDeptId, deptId);

        Page page = sysUserDao.selectPage(new Page<>(current, size), queryWrapper);
        System.out.println("总页数： "+page.getPages());
        return page;
    }


    // 此处put 由问题 插入的sysUser id为空 待解决
    @CacheEvict(value = "SysUserList",key = "userist")
    @Override
    public int insert(SysUser sysUser) {
        return sysUserDao.insert(sysUser) > 0 ? 1 : 0;
    }

    @Override
    public int selectUserCount() {
        return Math.toIntExact(sysUserDao.selectCount(null));
    }

    @CachePut(value = "SysUserById",key = "'user_'+#sysUser.getId()",condition = "#sysUser.getId() != null")
    @Override
    public SysUser updateByUser(SysUser sysUser) {
        UpdateWrapper<SysUser> updateWrapper = new UpdateWrapper();
        updateWrapper.lambda().eq(SysUser::getId, sysUser.getId());
        int update = sysUserDao.update(sysUser, updateWrapper);
        if (update > 0)
            return sysUserDao.selectById(sysUser.getId());
        return new SysUser();
    }
}
