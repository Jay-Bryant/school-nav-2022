package com.school.nav.controller;

import com.school.nav.entity.SysResources;
import com.school.nav.response.ResponseResult;
import com.school.nav.service.SysResourceService;
import com.school.nav.service.elasticSearch.SearchResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * FileName: index
 * Author: qinshixing
 * Data: 2022/3/3 0003
 * Description: 首页控制层
 */
@Slf4j
@Controller
public class Index {

    @Autowired
    private SearchResourceService searchResourceService;


    /**
     * 首页映射
     * @return
     */
    @RequestMapping(value = {"/","/index"},method = RequestMethod.GET)
    public String index() {
        return "index";
    }


    /**
     * 首页异步请求接口
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/get/indexInfo")
    public ResponseResult indexInfo() {
        SearchHits<SysResources> search = searchResourceService.search(null, true, false, false, 0);
        List<SysResources> resourcesList = new ArrayList<>();
        search.getSearchHits().forEach((e)->{
            resourcesList.add(e.getContent());
        });
        return ResponseResult.SUCCESS(resourcesList);
    }

}
