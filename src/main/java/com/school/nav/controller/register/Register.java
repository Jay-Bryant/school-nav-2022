package com.school.nav.controller.register;

import com.school.nav.entity.SysUser;
import com.school.nav.response.ResponseResult;
import com.school.nav.service.SysUserService;
import com.school.nav.utils.SaltUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.mgt.SecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * FileName: register
 * Author: qinshixing
 * Data: 2022/3/3 0003
 * Description: 注册控制层
 */
@Slf4j
@Controller
public class Register {

    @Value("${setHashIterations}")
    private int HashIterations;

    @Autowired
    private SysUser sysUser;

    @Autowired
    private SaltUtil saltUtil;

    @Autowired
    private SysUserService sysUserService;

    /**
     * 注册地址映射
     * @return
     */
    @RequestMapping(value = "/register",method = RequestMethod.GET)
    public String registerMap() {
        return "register";
    }


    /**
     * 注册异步请求接口
     * @param username
     * @param password
     * @return
     */
    @ResponseBody
    @RequiresRoles(value = "sys:update")
    @RequestMapping(value = "/register/info",method = RequestMethod.POST)
    public ResponseResult toRegister(@RequestParam("username") String username,
                                     @RequestParam("password") String password) {

        System.out.println(sysUser);
        SysUser user = sysUserService.selectOne("login_name", username);
        if (user == null) {   // 数据库无重复登录名
            sysUser.setLoginName(username);
            String salt = saltUtil.getSalt();
            sysUser.setSalt(salt);
            SecurityManager securityManager = SecurityUtils.getSecurityManager();
            //DefaultPasswordService defaultPasswordService = new DefaultPasswordService;
            String simpleHash = String.valueOf(new SimpleHash("md5", password, salt, HashIterations));
            sysUser.setPassword(simpleHash);
            int insert = sysUserService.insert(sysUser);
            if (insert > 0)
                return ResponseResult.SUCCESS_CUSTOM("注册成功", null);
            return ResponseResult.FAIL_CUSTOM("error，请联系管理员", null);
        }
        return ResponseResult.FAIL_CUSTOM("此用户名已存在", null);
    }
}
