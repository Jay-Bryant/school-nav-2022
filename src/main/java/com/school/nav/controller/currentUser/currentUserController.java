package com.school.nav.controller.currentUser;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.school.nav.entity.SysResources;
import com.school.nav.entity.SysUser;
import com.school.nav.response.ResponseResult;
import com.school.nav.service.SysResourceService;
import com.school.nav.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * FileName: currentUserController
 * Author: qinshixing
 * Data: 2022/3/11 0011
 * Description: 当前用户操作类
 */
@Slf4j
@Controller
public class currentUserController {

    @Autowired
    private SysResourceService resourceService;

    @Autowired
    private SysUserService sysUserService;
    /**
     * 获取当前用户推荐的有效资源 链表
     * @param currentPage
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/currentUser/getPostResource",method = RequestMethod.POST)
    public ResponseResult selectListByAuthorId(@RequestBody int currentPage) {
        SysUser currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        IPage<SysResources> authorResources = resourceService.selectListByAuthorId(String.valueOf(currentUser.getId()), 2, currentPage);
        List<SysResources> userResourcesList = authorResources.getRecords();
        if (!userResourcesList.isEmpty())
            return ResponseResult.SUCCESS(userResourcesList);
        else
            return ResponseResult.SUCCESS_CUSTOM("你未推荐有效资源", null);
    }


    /**
     * 获取当前用户的相关信息 通过shiro 获取
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/currentUser/getMyInfo",method = RequestMethod.POST)
    public ResponseResult getMyInfo() {
        // 设置策略  在shiro 里加入当前用户的一些信息
        SysUser currentUser = null;
        try {
            currentUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        } catch (Exception e) {
            // 获取不到用户 则去登录   这个在 shiro 加入相关的权限即可实现
            e.printStackTrace();
        }
        return ResponseResult.SUCCESS(currentUser);
    }
}
