package com.school.nav.controller.Administrators.Security;

import com.school.nav.service.IpBlockService;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * FileName: SecurityController
 * Author: qinshixing
 * Data: 2022/3/9 0009
 * Description: 管理员安全控制层
 */
@Controller(value = "/sys/security")
//@RequiresRoles("")
public class SecurityController {

    @Autowired
    private IpBlockService ipBlockService;

    /**
     * iP封杀
     * @param ip
     * @return
     */
    public void IpBlock(String ip) {
        ipBlockService.insertIpBlock(ip);
    }

    public void IpBlockList(List<String> list) {
        ipBlockService.insertByIpList(list);
    }
}
