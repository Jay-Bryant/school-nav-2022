package com.school.nav.controller.Administrators;

import com.school.nav.response.ResponseResult;
import com.school.nav.service.SysResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * FileName: Administrators
 * Author: qinshixing
 * Data: 2022/3/8 0008
 * Description: 管理员控制层
 */
@Controller
public class UpdateResourceStatusController {
    @Autowired
    private SysResourceService sysResourceService;

    //@RequiresRoles()
    @ResponseBody
    @RequestMapping(value = "/sys/updateStatus", method = RequestMethod.POST)
    public ResponseResult updateResourceStatus(@RequestParam("id") String  id, @RequestParam("status") Integer status) {
        try {
            sysResourceService.updateResourceStatus(String.valueOf(id), status);
            return ResponseResult.SUCCESS_CUSTOM("状态更新成功", null);
        } catch (Exception e) {
            return ResponseResult.FAIL_CUSTOM("状态更新失败", null);
        }

    }
}
