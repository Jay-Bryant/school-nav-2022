package com.school.nav.controller.login;

import com.school.nav.entity.SysUser;
import com.school.nav.response.ResponseResult;
import com.school.nav.service.SysUserService;
import com.school.nav.utils.IpUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * FileName: Login
 * Author: qinshixing
 * Data: 2022/3/3 0003
 * Description: 登录控制层
 */
@Slf4j
@Controller
public class Login {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 登录地址映射
     *
     * @return
     */
    @RequestMapping(value = "/login")
    public String loginMap(HttpServletRequest request) {
        String ipAddr = IpUtil.getIpAddr(request);
        System.out.println(ipAddr);
        System.out.println(IpUtil.getLocalIP());
        return "login";
    }


    /**
     * 加个IP封杀
     *
     * @param username
     * @param password
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/loginInfo", method = RequestMethod.GET)
    public ResponseResult toLogin(@RequestParam("username") String username,
                                  @RequestParam("password") String password) {
        Subject currentUser = SecurityUtils.getSubject();   //shiro 获取用户

        if (!currentUser.isAuthenticated()) {  //未被认证的用户
            UsernamePasswordToken token = new UsernamePasswordToken(username, password); //token

            try {
                String localIP = IpUtil.getLocalIP();
                currentUser.login(token);
                SysUser user = new SysUser();
                SysUser loginUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
                user.setId(loginUser.getId());
                if (localIP != null) {
                    user.setLoginIp(localIP);
                    log.info("登录用户ID={},最后登录IP地址为:{}", loginUser.getId(), localIP);
                } else {
                    log.info("登录用户ID={},获取IP失败", loginUser.getId());
                }
                user.setStatus('1');
                sysUserService.updateByUser(user);
                System.out.println(sysUserService.selectById(String.valueOf(user.getId())));
            } catch (IncorrectCredentialsException e) {
                return ResponseResult.FAIL_CUSTOM("密码错误", null);
            } catch (UnknownAccountException e) {
                return ResponseResult.FAIL_CUSTOM("账号异常", null);
            } catch (NullPointerException e) {
                return ResponseResult.FAIL_CUSTOM("账号不存在", null);
            }
        }
        return ResponseResult.SUCCESS_CUSTOM("登录成功", null);
    }
}
