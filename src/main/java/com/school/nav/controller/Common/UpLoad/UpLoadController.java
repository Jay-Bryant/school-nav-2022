package com.school.nav.controller.Common.UpLoad;

import com.school.nav.entity.SysUser;
import com.school.nav.response.ResponseResult;
import com.school.nav.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * FileName: UpLoadController
 * Author: qinSX
 * Data: 2022/04/12 -- 22:35
 * Description: 上传控制层
 */
@Slf4j
@RestController
public class UpLoadController {

    @Autowired
    SysUserService sysUserService;

    /**
     * 单文件上传
     *
     * @param request
     * @param file
     * @return
     */
    @RequestMapping(value = "/toUpLoad", method = RequestMethod.GET)
    public ResponseResult upLoad(HttpServletRequest request, @RequestParam("file") MultipartFile file) {

        // 测试MultipartFile接口的各个方法
        System.out.println("文件类型ContentType=" + file.getContentType());
        System.out.println("文件组件名称Name=" + file.getName());
        System.out.println("文件原名称OriginalFileName=" + file.getOriginalFilename());
        System.out.println("文件大小Size=" + file.getSize() / 1024 + "KB");
        try {
            if (file.isEmpty()) {
                return ResponseResult.FAIL_CUSTOM("文件为空", null);
            }
            // 获取文件名
            String fileName = file.getOriginalFilename();
            log.info("上传的文件名为：" + fileName);
            // 获取文件的后缀名
            String suffixName = fileName.substring(fileName.lastIndexOf("."));
            log.info("文件的后缀名为：" + suffixName);

            // 获取文件相对类路径
            //String filePath = request.getServletContext().getRealPath("/");
            //文件绝对路径,项目中一般使用相对类路径,即使文件变更路径也会跟着变
            String filePath = request.getServletContext().getRealPath("G:\\dev_workspace\\springboot-learning-examples\\springboot-13-fileupload\\src\\main\\resources\\static");
            System.out.println("path = " + filePath);
            //构造一个路径
            String newImg = UUID.randomUUID() + suffixName;
            String path = filePath + newImg;
            log.info("构造路径" + path);

            File dest = new File(path);
            // 检测是否存在目录
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();// 新建文件夹

            }
            file.transferTo(dest);// 文件写入
            SysUser user = new SysUser();
            user.setAvatar(path);
            sysUserService.updateByUser(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseResult.SUCCESS("上传成功");
    }
}
