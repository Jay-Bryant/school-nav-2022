package com.school.nav.controller.search;

import com.school.nav.entity.SearchInfo;
import com.school.nav.entity.SysResources;
import com.school.nav.entity.SysUser;
import com.school.nav.response.ResourceStatus;
import com.school.nav.response.ResponseResult;
import com.school.nav.service.SysResourceService;
import com.school.nav.service.elasticSearch.SearchResourceService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * FileName: ElasticSearch
 * Author: qinshixing
 * Data: 2022/3/5 0005
 * Description: 搜索控制层
 */
@Controller
public class ElasticSearchResource {

    @Autowired
    private SearchResourceService searchResourceService;

    @Autowired
    private SysResourceService resourceService;

    @ResponseBody
    @RequestMapping(value = "/resources",method = RequestMethod.POST)
    public ResponseResult search(@RequestBody SearchInfo searchInfo) {

        if (searchInfo.getPerfect() == null)
            searchInfo.setPerfect(false);
        if (searchInfo.getSortByCollection() == null)
            searchInfo.setSortByCollection(false);
        if (searchInfo.getSortByTime() == null)
            searchInfo.setSortByTime(false);
        if (searchInfo.getCurrentPage() == null)
            searchInfo.setCurrentPage(0);

        System.out.println(searchInfo);
        SearchHits<SysResources> searchHits = searchResourceService.search(
                searchInfo.getTags(),
                searchInfo.getSortByTime(),
                searchInfo.getSortByCollection(),
                searchInfo.getPerfect(),
                searchInfo.getCurrentPage());
        List<SearchHit<SysResources>> searchHitList = searchHits.getSearchHits();
        List<SysResources> resourcesList = new ArrayList<>();
        searchHitList.forEach(sysResourcesSearchHit -> {
            resourcesList.add(sysResourcesSearchHit.getContent());
        });
        return ResponseResult.SUCCESS(resourcesList);
    }


    @ResponseBody
    @RequestMapping("/rd/add")
    public ResponseResult addResources(@RequestBody SysResources sysResources) {
        Subject subject = SecurityUtils.getSubject();
        SysUser currentUser = (SysUser) subject.getPrincipal();
        System.out.println(currentUser.getId());

        sysResources.setUserId(currentUser.getId());
        sysResources.setPerfect(0);

        List<SysResources> byLink = searchResourceService.findByLink(sysResources.getLink());
        byLink.forEach((e)->System.out.println(e));

        if (searchResourceService.findByLink(sysResources.getLink()).isEmpty()) {
            int insert = resourceService.insert(sysResources);
            if (insert == 1)
                return ResponseResult.SUCCESS_CUSTOM("成功提交,感谢你的分享", ResourceStatus.VERIFICATION_STATUS);
            return ResponseResult.FAIL_CUSTOM("提交失败 请联系管理员", null);
        } else
            return ResponseResult.FAIL_CUSTOM("此资源链接已存在,感谢你的分享", null);
    }
}
