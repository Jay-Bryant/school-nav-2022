package com.school.nav.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * FileName: RoleUtil
 * Author: qinshixing
 * Data: 2022/3/1 0001
 * Description: 权限字符工具
 */
public class RoleUtil {
    public static List<String> getStringList(String s) {
        ArrayList<String > list = new ArrayList<>();
        String[] split = s.split(";");
        for (int i = 0; i < split.length; i++)
            list.add(split[i]);
        return list;
    }
}
