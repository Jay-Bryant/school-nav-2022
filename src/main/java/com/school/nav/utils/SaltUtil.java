package com.school.nav.utils;

import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * FileName: SaltUtil
 * Author: qinshixing
 * Data: 2022/2/5 00059:20
 * Description: 盐值生成器
 */
@Component
public class SaltUtil {
    /**
     * 盐值的个数
     * 默认为4
     */
    private final int num = 4;

    /**
     * 字符数据集
     */
    private final String stringData = "abcdefghijklmnopqrstuvwxyzABCDEFGHIGKLMNOPQRSTUVWXYZ1234567890./";


    public String getSalt() {
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < this.num; i++)
            stringBuilder.append(this.stringData.charAt(random.nextInt(64)));   //不包含bound的整数
        return String.valueOf(stringBuilder);
    }

}
