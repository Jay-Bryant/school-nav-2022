package com.school.nav.response;

import lombok.Getter;

/**
 * FileName: CodeStatus
 * Author: qinshixing
 * Data: 2022/2/6
 * Description: 状态码枚举类
 */
@Getter
public enum CodeStatus {


    FAIL_404(404, "404错误"),
    FAIL_500(500, "服务器异常"),
    FAIL_400(400, "请求无效"),
    LOGIN_SUCCESS(200, "登录成功"),
    UPDATE(1102,"MESSAGE-UPDATE"), //用户信息或权限已更新（退出重新登录）
    SUCCESS(200,"数据获取成功");

    /**
     * 枚举构造方法
     * @param code 状态码
     * @param message 信息
     */
    CodeStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private final Integer code;
    private final String message;

}



