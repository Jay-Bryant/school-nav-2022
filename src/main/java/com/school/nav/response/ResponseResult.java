package com.school.nav.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @Title: ResponseResult
 * @Description: 前端请求响应结果,code:编码,message:描述,obj对象，可以是单个数据对象，数据列表或者PageInfo
 * @author: qinshixing
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class ResponseResult implements Serializable {

    /**
     * 状态码
     */
    private Integer code;
    private String message;
    private Object data;

    /**
     * 成功
     * @param object
     * @return 成功数据
     */
    public static ResponseResult SUCCESS(Object object) {
        return new ResponseResult(CodeStatus.SUCCESS.getCode(), CodeStatus.SUCCESS.getMessage(), object);
    }

    /**
     * 404错误
     * @return FAIL_404数据
     */
    public static ResponseResult FAIL_404() {
        return new ResponseResult(CodeStatus.FAIL_404.getCode(), CodeStatus.FAIL_404.getMessage(), null);
    }

    /**
     * 500错误
     * @return FAIL_500数据
     */
    public static ResponseResult FAIL_500() {
        return new ResponseResult(CodeStatus.FAIL_500.getCode(), CodeStatus.FAIL_500.getMessage(), null);
    }

    /**
     * 自定义成功数据模式
     * @param message
     * @param data
     * @return
     */
    public static ResponseResult SUCCESS_CUSTOM(String message,Object data) {
        return new ResponseResult(CodeStatus.SUCCESS.getCode(), message, data);
    }

    /**
     * 自定义失败数据模式
     * @param message
     * @param data
     * @return
     */
    public static ResponseResult FAIL_CUSTOM(String message, Object data) {
        return new ResponseResult(CodeStatus.FAIL_400.getCode(), message, data);
    }

}
