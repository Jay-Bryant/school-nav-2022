package com.school.nav.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Title: PageDataResult
 * @Description: 封装DTO分页数据（记录数和所有记录）
 * @author: qinshixing
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class PageDataResult {

    /**
     * 状态码
     */
    private Integer code=200;

    /**
     * 总数
     */
    private Integer totals;

    /**
     * 数据集合
     */
    private List<?> list;



    public static PageDataResult SUCCESS(int totals, List<?> list) {
        return new PageDataResult(CodeStatus.SUCCESS.getCode(), totals, list);
    }

    public static PageDataResult FAIL() {
        return new PageDataResult(CodeStatus.FAIL_400.getCode(), null, null);
    }


}
