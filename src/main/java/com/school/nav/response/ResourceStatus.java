package com.school.nav.response;

import lombok.Data;

/**
 * FileName: resourceStatus
 * Author: qinshixing
 * Data: 2022/3/8 0008
 * Description: 资源状态码
 */
public enum ResourceStatus {
    /**
     * 资源审核中 状态码：0
     */
    VERIFICATION_STATUS(0, "资源审核中"),
    /**
     * 审核通过 状态码：1
     */
    PASS_STATUS(1, "审核通过"),
    /**
     * 资源未被通过审核 状态码：2
     */
    FAIL_STATUS(2, "资源未被通过审核");



    private final Integer code;
    private final String message;

    ResourceStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
