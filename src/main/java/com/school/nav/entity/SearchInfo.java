package com.school.nav.entity;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

/**
 * FileName: DataInfo
 * Author: qinshixing
 * Data: 2022/2/28 0028
 * Description: 搜索数据信息实体
 */
@Data
public class SearchInfo {

    /**
     * 标签多参数
     */
    private String[] tags;

    /**
     * 按时间排序
     */
    @Value("false")
    private Boolean sortByTime;

    /**
     * 按收藏数排序
     */
    @Value("false")
    private Boolean sortByCollection;

    /**
     * 是否为精选
     */
    @Value("false")
    private Boolean perfect;

    /**
     * 当前页
     */
    @Value("0")
    private Integer currentPage;


    public static void main(String[] args) {
        SearchInfo searchInfo = new SearchInfo();
        searchInfo.setTags(new String[]{"java", "通信工程", "后端"});
        searchInfo.setSortByTime(true);
        searchInfo.setSortByCollection(false);
        searchInfo.setPerfect(true);
        searchInfo.setCurrentPage(1);

        Object json = JSON.toJSON(searchInfo);
        System.out.println(json);
    }
}
