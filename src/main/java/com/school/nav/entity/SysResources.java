package com.school.nav.entity;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.io.Serializable;


/**
 * 资源表单
 * (SysResources)实体类
 * @author makejava
 * @since 2022-03-02 21:01:38
 */
@Data
@Component
@TableName(value = "sys_resources")
@Document(indexName = "resources_data")
public class SysResources implements Serializable {
    private static final long serialVersionUID = -20642621717548453L;
    /**
    * 主键ID
    */
    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "resources_id",type = IdType.ASSIGN_ID)
    @Field(type = FieldType.Long)
    private Long id;
    /**
    * 上传用户ID
    */
    @JsonSerialize(using = ToStringSerializer.class)
    @Field(type = FieldType.Keyword)
    private Long userId;
    /**
    * 资源标题
    */
    @TableField(value = "title")
    private String title;
    /**
    * 描述
    */
    @TableField(value = "description")
    private String description;       //注意此处describe 为sql关键字 不能使用改之
    /**
    * 详情
    */
    @TableField(value = "details")
    private String details;
    /**
    * 资源链接
    */
    @TableField(value = "link")
    @Field(type = FieldType.Keyword)
    private String link;
    /**
    * 资源标签
    */
    @Field(type = FieldType.Text,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")  //分词器 ik_max_word
    @TableField(value = "tag",typeHandler = JacksonTypeHandler.class)
    private String[] tag;
    /**
     * 是否为精选(0为false 1为true)
     */
    private Integer perfect;
    /**
    * 资源图标url
    */
    @TableField(value = "icon_url")
    @Field(type = FieldType.Keyword)
    private String iconUrl;
    /**
    * 收藏总数
    */
    @TableField(value = "collect_nums")
    private Integer collectNums;
    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    private Date creatDate;
    /**
    * 资源状态（0 审核 1审核通过 2 审核失败）由管理员更改
    */
    private Integer status;
    /**
    * 备注
    */
    private String remark;

    public static void main(String[] args) {
        SysResources sysResources = new SysResources();
        sysResources.setPerfect(0);
        sysResources.setCollectNums(24);
        sysResources.setTag(new String[]{"秦世兴", "张成", "贾梦豪", "黄俊升", "敖鹏举"});
        sysResources.setDescription("测试 测试");
        sysResources.setDetails("操 喜欢是拦不住的");
        sysResources.setIconUrl("http://www.baidu.com");
        sysResources.setLink("http://www.bilibili.com");
        sysResources.setTitle("你好 2022");
        Object json = JSON.toJSON(sysResources);
        System.out.println(json);
    }

}