package com.school.nav.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.io.Serializable;

/**
 * 用户信息表(SysUser)实体类
 * 主键 雪花算法
 * @author makejava
 * @since 2022-02-27 19:37:58
 */
@Data
@TableName("sys_user")
@Component
@Document(indexName = "sys_user")
public class SysUser implements Serializable {
    private static final long serialVersionUID = -21080249038939220L;
    /**
    * 用户ID
    */
    @Id
    @TableId(type = IdType.ASSIGN_ID,value = "user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @Field(type = FieldType.Long)
    private Long id;
    /**
    * 部门ID
    */
    @Field(type = FieldType.Keyword)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long deptId;
    /**
    * 登录账号
    */
    @Field(type = FieldType.Keyword)
    private String loginName;
    /**
    * 用户昵称
    */
    @Field(type = FieldType.Keyword)
    private String userName;
    /**
    * 用户权限（0系统1注册用户）
    */
    private Integer userRoleId;
    /**
    * 邮箱
    */
    @Field(type = FieldType.Keyword)
    private String email;
    /**
    * 电话
    */
    @TableField(value = "phonenumber")
    private String phoneNumber;
    /**
    * 性别（0女1男）
    */
    private Character sex;
    /**
    * 密码
    */
    private String password;
    /**
    * 盐
    */
    private String salt;
    /**
    * 帐号状态（0正常 1停用）
    */
    @Value("${userStatus}")
    private Character status;
    /**
    * 最后登录IP
    */
    @Field(type = FieldType.Ip)
    private String loginIp;
    /**
    * 最后登录时间
    */
    @Field(type = FieldType.Date)
    private Date loginDate;
    /**
    * 账号创建时间
    */
    @Field(type = FieldType.Date)
    @TableField(fill = FieldFill.INSERT)
    private Date creatDate;
    /**
    * 账号更新时间
    */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;
    /**
    * 备注
    */
    private String remark;
    /**
    * 头像路径
    */
    @Field(type = FieldType.Keyword)
    private String avatar;
}