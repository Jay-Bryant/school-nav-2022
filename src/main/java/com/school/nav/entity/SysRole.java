package com.school.nav.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统权限表(SysRole)实体类
 *
 * @author makejava
 * @since 2022-03-01 15:36:12
 */
@Data
@TableName(value = "sys_role")
public class SysRole implements Serializable {
    private static final long serialVersionUID = -17233883288876478L;
    /**
    * 主键 自增
    */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
    * 权限ID
    */
    private Integer sysRoleId;
    /**
    * 权限符
    */
    private String roles;
    /**
    * 备注
    */
    private String remark;



}