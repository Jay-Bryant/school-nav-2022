package com.school.nav.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * FileName: IpBlock
 * Author: qinshixing
 * Data: 2022/3/9 0009
 * Description: IP封杀类
 */
@Data
@TableName(value = "ip_bock")
public class IpBlock {

    @TableId(value = "id",type = IdType.AUTO)   //自增主键
    private Long id;

    private String ip;

    @TableField(fill = FieldFill.INSERT)
    private Date creatDate;

    private String remark;
}
