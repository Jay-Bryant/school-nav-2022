package com.school.nav.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/**
 * 用户收藏类(LikeOfResources)实体类
 *
 * @author makejava
 * @since 2022-03-03 14:28:12
 */
@Data
@TableName("like_of_resources")
@Document(indexName = "like_resources")    //收藏的资源
public class LikeOfResources implements Serializable {
    private static final long serialVersionUID = 125379904498675339L;
    /**
    * 收藏likeID
    */
    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "like_id",type = IdType.ASSIGN_ID)
    private Long id;
    /**
    * 资源ID
    */
    @JsonSerialize(using = ToStringSerializer.class)
    @Field(type = FieldType.Keyword)
    private Long resourcesId;
    /**
    * 收藏者ID
    */
    @JsonSerialize(using = ToStringSerializer.class)
    @Field(type = FieldType.Keyword)
    private Long userId;
    /**
    * 状态
    */
    private Integer status;
    /**
    * 备注
    */
    private String remark;

}