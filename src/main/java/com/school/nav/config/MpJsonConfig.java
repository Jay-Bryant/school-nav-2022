package com.school.nav.config;

import com.baomidou.mybatisplus.extension.handlers.GsonTypeHandler;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.CommandLineRunner;

/**
 * FileName: MpJsonConfig
 * Author: qinshixing
 * Data: 2022/3/6 0006
 * Description: MpJsonConfig 用于数组存在数据库中
 */
public class MpJsonConfig implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        JacksonTypeHandler.setObjectMapper(new ObjectMapper());
    }
}
