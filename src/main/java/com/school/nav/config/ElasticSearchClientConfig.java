package com.school.nav.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;

/**
 * FileName: ElasticSearchClientConfig
 * Author: qinshixing
 * Data: 2022/2/16 0016
 * Description: ElasticSearch配置类
 */
@Configuration
public class ElasticSearchClientConfig extends AbstractElasticsearchConfiguration {

    /* @Bean
     public RestHighLevelClient restHighLevelClient() {
         RestHighLevelClient restHighLevelClient = new RestHighLevelClient(
                 RestClient.builder(new HttpHost("127.0.0.1",9200,"http")));
         return restHighLevelClient;

     }*/
    @Value("${elasticHost}")
    String elasticHost;

    @Bean
    @Override
    public RestHighLevelClient elasticsearchClient() {
        final ClientConfiguration clientConfiguration =  ClientConfiguration.builder()
                .connectedTo(elasticHost)
                .build();
        return RestClients.create(clientConfiguration).rest();
    }
}
