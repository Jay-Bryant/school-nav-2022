package com.school.nav.config;

import com.school.nav.entity.SysUser;
import com.school.nav.service.SysRoleService;
import com.school.nav.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * FileName: MyShiro
 * Author: qinshixing
 * Data: 2022/2/1 000114:27
 * Description: shiro 配置参数
 */
@Slf4j
public class MyShiro extends AuthorizingRealm {

    @Autowired
    SysUserService sysUserService;

    @Autowired
    SysRoleService sysRoleService;
    // 授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        log.info("权限配置-->MyShiroRealm.doGetAuthorizationInfo()");
        // 简单的授权信息构造器 源码解析 SimpleAuthorizationInfo
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();

        Subject currentUser = SecurityUtils.getSubject();
        SysUser sysUser = (SysUser)currentUser.getPrincipal();
        System.out.println(sysUser.toString());
        simpleAuthorizationInfo.addRoles(sysRoleService.getRoleList(sysUser.getUserRoleId()));
        //simpleAuthorizationInfo.addRole(String.valueOf(sysUser.getRoleId()));
        //角色属性
        //simpleAuthorizationInfo.addStringPermissions(sysUser.getPermissions());

        return simpleAuthorizationInfo;
    }

    /**
     * 认证  也就是说验证用户输入的账号和密码是否正确
     * @param authenticationToken   这里的Token 实在controller层上传入的Token 后走这一层调用认证的方法
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        log.info("认证-->ShiroRealm.doGetAuthenticationInfo()");
        UsernamePasswordToken token = (UsernamePasswordToken)authenticationToken;  //转化为UsernamePasswordToken  因为前面传来的是这个类型
        String username = token.getUsername();
        SysUser sysUser = sysUserService.selectOne("login_name",username);
        if (sysUser == null)
            return null;
        // 认证器  简单的认证构造器 SimpleAuthenticationInfo
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                sysUser,
                sysUser.getPassword(),
                ByteSource.Util.bytes(sysUser.getSalt()),
                sysUser.getLoginName());
        return authenticationInfo;
    }
}
